package ru.vmaksimenkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.model.Project;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.System.out;

public final class ProjectListCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show project list";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-list";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        out.println("[PROJECT LIST]");
        out.printf("\t| %-36s | %-12s | %-20s | %-30s | %-30s | %-30s %n", "ID", "STATUS", "NAME", "CREATED", "STARTED", "FINISHED");
        @NotNull AtomicInteger index = new AtomicInteger(1);
        @Nullable final List<Project> list = serviceLocator.getProjectService().findAll(userId);
        if (list == null) return;
        list.forEach((x) -> out.println(index.getAndIncrement() + "\t" + x));
    }

}
