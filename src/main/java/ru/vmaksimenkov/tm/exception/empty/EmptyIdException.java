package ru.vmaksimenkov.tm.exception.empty;

import ru.vmaksimenkov.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error! ID is empty...");
    }

}
